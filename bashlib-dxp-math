# ---------------------------------------------------------------------
# Script   : bashlib-dxp-math
# Descrição: Coleção de funções matemáticas úteis em scripts em Bash.
# Versão   : 0.0.1
# Data     : 22.08.2020
# Licença  : GNU/GPL v3.0
# ---------------------------------------------------------------------
# Aviso! Ainda em desenvolvimento, não use em produção!
# ---------------------------------------------------------------------
# Copyright (C) 2020  Blau Araujo <blau@debxp.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
# Uso: dxp_include math
# ---------------------------------------------------------------------
#
# Colaboradores:
#
#   @robsonalexandre
#
# -------------------------------------------------------------------
# Função   : ceil()
# Descrição: arredonda números de ponto flutuante para cima.
# Uso      : ceil <número com ponto flutuante>
# -------------------------------------------------------------------
ceil() {
    awk -v n=$1 'BEGIN {
        if (n ~ /\.[1-9]+/) {
            printf "%d\n", n + 1
        } else {
            print int(n)
        }
    }'
}
# -------------------------------------------------------------------
# Função   : round()
# Descrição: arredonda números de ponto flutuante.
# Uso      : round <número com ponto flutuante>
# -------------------------------------------------------------------
round() { awk -v n=$1 'BEGIN { printf "%d\n", n + 0.5 }'; }
# -------------------------------------------------------------------
# Função   : floor()
# Descrição: arredonda números de ponto flutuante para baixo.
# Uso      : ceil <número com ponto flutuante>
# -------------------------------------------------------------------
floor() { awk -v n=$1 'BEGIN { print int(n) }'; }
# -------------------------------------------------------------------
# Função   : calc()
# Descrição: executa cálculos aritméticos.
# Uso      : calc '<expressão>' [<precisão>]
# -------------------------------------------------------------------
calc() { awk 'BEGIN { printf "%.'${2:-2}'f\n", '"$1"'}'; }
# -------------------------------------------------------------------
# Função   : maxvalb()
# Descrição: maior inteiro possível dado um número de bytes.
# Uso      : maxvalb <bytes>
# -------------------------------------------------------------------
maxvalb() { echo $((2**(8*${v:-1})-1)); }
# -------------------------------------------------------------------
# Função   : is_prime()
# Descrição: retorna sucesso se o número for primo.
# Uso      : is_prime <número>
# -------------------------------------------------------------------
is_prime() {
    local num
    num=$(($1/2))
    while [ $num -gt 1 ]; do
        (($1%num==0)) && return 1
        ((num--))
    done
    return 0
}
# -------------------------------------------------------------------
# Função   : is_hex()
# Descrição: verifica se o número é um hexadecimal válido.
# Uso      : is_hex <número em hexa>
# -------------------------------------------------------------------
is_hex() [[ "$1" =~ ^[[:xdigit:]]+$ ]]
# -------------------------------------------------------------------
# Função   : is_bin()
# Descrição: verifica se o número é um binário válido
# Uso      : is_bin <número>
# -------------------------------------------------------------------
is_bin() [[ $1 =~ ^[01]+$ ]]
# -------------------------------------------------------------------
# Função   : is_int()
# Descrição: verifica se a string é um inteiro válido.
# Uso      : is_int <string>
# -------------------------------------------------------------------
is_int() [[ "$1" =~ ^[-+]?[0-9]+([.,][0-9]+)?$ ]]
# -------------------------------------------------------------------
# Função   : is_doub()
# Descrição: verifica se a string é um número de ponto flutuante válido.
# Uso      : is_doub <string>
# -------------------------------------------------------------------
is_doub() [[ "$1" =~ ^[-+]?[0-9]+([.,][0-9]+)?$ ]]
# -------------------------------------------------------------------
# Função   : is_fib()
# Descrição: verifica se o inteiro está na série de fibonacci.
# Uso      : is_fib <número>
# -------------------------------------------------------------------
isfib() {
    local fib=(1 1) num=1
    while [ $num -le $1 ]; do
        expr=${fib[@]: -2}
        num=$((${expr// /+}))
        fib+=($num)
        [ $num -eq $1 ] && return 0
    done
    return 1
}
# -------------------------------------------------------------------
# Função   : is_number()
# Descrição: verifica se a string é um número decimal válido.
# Uso      : is_number <string>
# -------------------------------------------------------------------
# is_number() [TODO]
# -------------------------------------------------------------------
# Função   : is_octal()
# Descrição: verifica se o número é um octal válido
# Uso      : is_bin <número>
# -------------------------------------------------------------------
# is_octal() [TODO]
